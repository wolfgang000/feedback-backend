# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :feedback,
  ecto_repos: [Feedback.Repo]

# Configures the endpoint
config :feedback, FeedbackWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "s3blbxLuo0P1rUUNLZGNqykQb4sDT6bA/iZiJOy1QgXQwQTmmg1uw9s2hfalcepP",
  render_errors: [view: FeedbackWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Feedback.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "Js+uCDeO"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
