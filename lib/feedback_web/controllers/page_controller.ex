defmodule FeedbackWeb.PageController do
  use FeedbackWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
